var elementArray = [];
var inn = 0;  

//Lager et element objekt
function element(elemName, elemMail, elemAddress) 
{
    this.name = elemName;
    this.mail = elemMail;
    this.address = elemAddress;
}

//Oppretter et nytt element, lagrer det i elementArray[] og sier ifra at det skal opprettes en rute. 
function setToElement()
{
   var newElem = new element(document.getElementById("navn").value, document.getElementById("mailen").value, document.getElementById("adressen").value);
    elementArray[inn] = newElem;
    addRute();
    inn++;
    document.getElementById('navn').value='';
    document.getElementById('mailen').value='';
    document.getElementById('adressen').value='';
}

//Opprette en ny rute med bilde og tekst fra input
function addRute()
{
    var div = document.createElement('div');
    
    div.className = 'rute';
    div.innerHTML = 
        "<img src= /indexPicture.png alt= personPlaceholder style= max-width:50%; height:auto;>" +
        "<p> Navn: </<p>" + elementArray[inn].name +
        "<p> E-Mail: </<p>" + elementArray[inn].mail + 
        "<p> Adresse:  </<p>" + elementArray[inn].address;

    document.getElementById("rutenett").appendChild(div);
}